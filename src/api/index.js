import AppConfig from '../config'l
import _ from 'lodash';
export class Api {
  fetch(url, params) {
    let fetchUrl = config.apiUrl;
    if (url) fetchUrl += url;
    const fetchParams = {
      method: params.method || 'GET',
    }
    Object.assign(fetchParams, params);
    if (_.isObject(params.body)) {
      fetchParams.body =  JSON.stringify(params.body);
    }
    return fetch(fetchUrl, fetchParams)
    .then(res => res.json())
  }
}