import React, { Component } from 'react';
import _ from 'lodash';
class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: props.columns,
      data: props.data,
    };
    if (!Array.isArray(props.data)) {
      this.state.data = [];
    }
    if (!Array.isArray(props.columns)) {
      const columns = [];
      _.forEach(this.state.data, (item) => {
        _.forEach(item, (value, key) => {
          console.log(_.find(this.state.colums, { key }));
          if (!_.find(columns, { key })) {
            console.log(`Добавляю ${key}`);
            columns.push({ title: key, key });
          }
        });
      });
      this.state.columns = columns;
    }
    this.renderValue = this.renderValue.bind(this);
  }
  renderValue(row, column) {
    if (column.key) {
      return _.get(row, column.key);
    } else if (typeof column.render === 'function') {
      return column.render(row);
    }
    return '';
  }
  render() {
    return (
      <table className="data-table__cnt">
        <thead>
          <tr className="data-table__hr">
            {this.state.columns.map((column, i) => {
              return <th key={`column_${i}`} className="data-table__h"><span>{column.title}</span></th>;
            })}
            {/* <th className="data-table__h text-center"><span>Целевые показатели</span></th>*/}

            {/* <th className="data-table__h text-center"><span>Ожидание</span></th>*/}
          </tr>
        </thead>
        <tbody>
          {this.state.data.map((row, i) => {
            return (<tr key={i} className="data-table__r">
              {this.state.columns.map((column) => {
                return (<td className="data-table__d"><span>{this.renderValue(row, column)}</span></td>);
              })}
            </tr>);
          })}
          {/* <tr className="data-table__r">*/}
          {/* <td className="data-table__d"><span>Выручка за акцию</span></td>*/}
          {/* <td className="data-table__d text-center"><span className="data-table__plus">14,1$</span></td>*/}
          {/* <td className="data-table__d text-center"><span>10$</span></td>*/}
          {/* </tr>*/}
          {/* <tr className="data-table__r">*/}
          {/* <td className="data-table__d"><span>Рентабильность капитала</span></td>*/}
          {/* <td className="data-table__d text-center"><span className="data-table__plus">23,2$</span></td>*/}
          {/* <td className="data-table__d text-center"><span>19$</span></td>*/}
          {/* </tr>*/}
          {/* <tr className="data-table__r">*/}
          {/* <td className="data-table__d"><span>Кредитный рейтинг</span></td>*/}
          {/* <td className="data-table__d text-center"><span className="data-table__minus">B-</span></td>*/}
          {/* <td className="data-table__d text-center"><span>BB+</span></td>*/}
          {/* </tr>*/}
          {/* <tr className="data-table__r">*/}
          {/* <td className="data-table__d data-table__d_last"><span>Имиджевый рейтинг</span></td>*/}
          {/* <td className="data-table__d text-center data-table__d_last"><span className="data-table__minus">39</span></td>*/}
          {/* <td className="data-table__d text-center data-table__d_last"><span>42</span></td>*/}
          {/* </tr>*/}
        </tbody>
      </table>
    );
  }
}


export default Table;
