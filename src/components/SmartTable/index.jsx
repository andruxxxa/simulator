import React, { Component } from 'react';
import _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
class SmartTable extends Component {
  static defaultProps = {
    findMethod: 'find',
    countMethod: 'count',
    sizePerPage: 10,
    limit: 10,
    offset: 0,
    columns: [],
    where: {},
    page: 1,
    sort: [],
  }
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataTotalSize: 0,
      page: props.page,
      sizePerPage: props.sizePerPage,
      filter: {
        limit: props.limit,
        offset: props.offset,
        where: props.where,
        sort: props.sort,
      },
    };
    this.renderValue = this.renderValue.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSizePerPageList = this.onSizePerPageList.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.onSortChange = this.onSortChange.bind(this);
  }
  componentDidMount() {
    this.updateData();
  }
  onSortChange(key, order) {
    console.log(key, order);
    const { filter, sizePerPage, dataTotalSize } = this.state;
    // if (order === 'desc') {
    //   console.log(111)
    //   filter.offset = dataTotalSize - sizePerPage;
    // } else if (order === 'asc') {
    //   console.log(222)
    //   filter.offset = 0;
    //   filter.limit = sizePerPage;
    // }
    console.log({ filter });
    filter.sort = [`${key} ${order}`];
    this.setState({ filter });
    return setTimeout(() => this.updateData(), 10);
  }
  onFilterChange(where) {
    console.log(where);
    const { filter } = this.state;
    const _where = {};
    _.forEach(where, ({ value }, key) => {
      _where[key] = {
        like: value,
      };
    });
    filter.where = _where;
    this.setState({ filter });
    return setTimeout(() => this.updateData(), 10);
  }
  onSizePerPageList(sizePerPage) {
    console.log('onSizePerPageList', sizePerPage);
    const { filter } = this.state;
    filter.limit = sizePerPage;
    this.setState({ filter, sizePerPage });
    return setTimeout(() => this.updateData(), 10);
  }
  onPageChange(page, sizePerPage) {
    console.log('onPageChange', { page, sizePerPage });
    const { filter } = this.state;
    filter.limit = sizePerPage;
    filter.offset = sizePerPage * (page - 1);
    this.setState({ filter, page });
    return setTimeout(() => this.updateData(), 10);
  }
  updateData() {
    return Promise.all([
      this.getData(),
      this.getDataTotalSize(),
    ]).then(([data, dataTotalSize]) => {
      console.log({ data, dataTotalSize });
      if (Array.isArray(data)) {
        this.setState({
          data, dataTotalSize,
        });
      }
    });
  }
  getData() {
    const filter = this.state.filter;
    let sortOrder = 'asc';
    const sortBy = _.last(filter.sort);
    if (sortBy && sortBy.indexOf('desc') !== -1) {
      sortOrder = 'desc';
    }
    const { sizePerPage, dataTotalSize, page } = this.state;
    console.log({ sizePerPage, dataTotalSize });
    if (sortOrder === 'desc') {
      let offset = dataTotalSize - filter.limit * page;
      if (offset < 0) {
        filter.limit += offset;
        offset = 0;
      }
      filter.offset = offset;
      // console.log('desc')
    } else if (sortOrder === 'asc') {
      console.log('asc');
      filter.offset = (page - 1) * sizePerPage;
      filter.limit = sizePerPage;
    }
    console.log(`Пропускаю ${filter.offset}, Лимит ${filter.limit}, Страница ${page}`);
    return this.props.model[this.props.findMethod]({ filter });
  }
  getDataTotalSize() {
    const { filter } = this.state;
    return this.props.model[this.props.countMethod]({ where: filter.where })
    .then((res) => {
      return _.get(res, 'count', 0);
    });
  }
  createColumns(data = {}) {
    const columns = [];
    _.forEach(data, (item) => {
      _.forEach(item, (value, key) => {
        if (!_.find(columns, { key })) {
          columns.push({ title: key, key });
        }
      });
    });
    this.setState({ columns });
  }
  renderValue(row, column) {
    if (column.key) {
      return _.get(row, column.key);
    } else if (typeof column.render === 'function') {
      return column.render(row);
    }
    return '';
  }
  render() {
    return (
      <div>
        {/* {JSON.stringify(this.state, null, 4)}*/}
        <BootstrapTable
          data={this.state.data}
          options={{
            page: this.state.page,
            onPageChange: this.onPageChange,
            onSizePerPageList: this.onSizePerPageList,
            sizePerPage: this.state.sizePerPage,
            onFilterChange: this.onFilterChange,
            onSortChange: this.onSortChange,
          }}
          fetchInfo={{ dataTotalSize: this.state.dataTotalSize }}
          remote
          pagination
        >
          {this.props.columns.map((column) => {
            return (<TableHeaderColumn {...column.options}>{column.title}</TableHeaderColumn>);
          })}
        </BootstrapTable>
      </div>
      // <table className="data-table__cnt">
      //   <thead>
      //     <tr className="data-table__hr">
      //       {this.state.columns.map((column, i) => {
      //         return <th key={`column_${i}`} className="data-table__h"><span>{column.title}</span></th>;
      //       })}
      //       {/* <th className="data-table__h text-center"><span>Целевые показатели</span></th>*/}
      //
      //       {/* <th className="data-table__h text-center"><span>Ожидание</span></th>*/}
      //     </tr>
      //   </thead>
      //   <tbody>
      //     {this.state.data.map((row, i) => {
      //       return (<tr key={i} className="data-table__r">
      //         {this.state.columns.map((column) => {
      //           return (<td className="data-table__d"><span>{this.renderValue(row, column)}</span></td>);
      //         })}
      //       </tr>);
      //     })}
      //   </tbody>
      // </table>
    );
  }
}


export default SmartTable;
