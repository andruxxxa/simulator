import { notify } from 'react-notify-toast';
export default function Notify(text = '', time = 3000, color = { background: '#0E1717', text: '#FFFFFF' }) {
  return notify.show(text, 'custom', time, color);
}
