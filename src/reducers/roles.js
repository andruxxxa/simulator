export default function (state = {}, action) {
  if (action.type === 'ROLES_INIT') {
    return action.data;
  }
  return state;
}
