import { combineReducers } from 'redux';
import roles from './roles';
import accounts from './accounts';
export default combineReducers({
  accounts,
  roles,
});
// export default function (state = {}, action) {
//   console.log(state, action);
//   if (action.type === 'USER_LOGIN') {
//     return {
//       ...state,
//       user: action.data,
//     };
//   }
//   return state;
// }
