import { push } from 'react-router-redux'
import cookies from 'browser-cookies';
import store from '../store';
export default function (state = {}, action) {
  if (action.type === 'ACCOUNTS_LOGIN') {
    cookies.set('tokenId', action.data.tokenId.toString());
    cookies.set('userId', action.data.user.id.toString());
    return {
      user: action.data.user,
      tokenId: action.data.tokenId,
    };
  } else if (action.type === 'ACCOUNTS_INIT') {
    return {
      user: action.data.user,
      tokenId: action.data.tokenId,
    };
  } else if (action.type === 'ACCOUNTS_LOGOUT') {
    cookies.erase('tokenId');
    cookies.erase('userId');
    return {
      user: null,
      tokenId: null,
    };
  }
  return state;
}
