import cookies from 'browser-cookies';
import Model from '../Model';
import store from '../../store';

const Account = new Model({
  modelName: 'Accounts',
  methods: {
    addToTeam: {
      url: ':userId/teams/:teamId',
      method: 'PUT', // option
      data: {
      },
    },
  },
});
Account.init = () => {
  const userId = cookies.get('userId');
  const tokenId = cookies.get('tokenId');
  console.log(`Ищу пользователя с userId=${userId}`);
  if (userId && tokenId) {
    return Account
    .findById(userId)
    .then((user) => {
      console.log('Мой пользователь', user);
      return store.dispatch({ type: 'ACCOUNTS_INIT', data: { tokenId, user } });
    });
  }
};
Account.getName = (user) => {
  let name = '';
  if (user.lastName) {
    name += user.lastName;
  }
  if (user.firstName) {
    if (name) name += ' ';
    name += user.firstName;
  }
  if (user.middleName) {
    if (name) name += ' ';
    name += user.middleName;
  }
  if (!name) {
    if (user.username) return user.username;
    return 'Пользователь';
  }
  return name;
};
export default Account;
