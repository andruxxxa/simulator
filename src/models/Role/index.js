import _ from 'lodash';
import Model from '../Model';
import store from '../../store';
const Role = new Model({ modelName: 'Roles' });
Role.init = () => {
  return Role.find()
  .then((roles) => {
    return store.dispatch({ type: 'ROLES_INIT', data: roles });
  });
};
Role.getById = (id) => {
  const state = store.getState();
  const { roles = [] } = state;
  return _.find(roles, { id });
};
Role.getByName = (name) => {
  const state = store.getState();
  const { roles = [] } = state;
  return _.find(roles, { name });
};
window.Role = Role;
export default Role;
