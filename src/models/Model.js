import cookies from 'browser-cookies';
import AppConfig from '../config';
import { generateServices, PropTypes, config } from './loopback-sdk-react';

export default class Model {
  constructor(props) {
    console.log(AppConfig.apiUrl);
    config.set('baseUrl', AppConfig.apiUrl);
    config.set('access_token', cookies.get('tokenId'));
    config.set('lang', 'ru');
    const model = new generateServices(
     props.modelName,
     Object.assign({},
       {
         find: {
           url: '/',
           method: 'GET', // option
           data: {
             filter: {
               limit: 10,
             }, // option
           },
         },
         findOne: {
           url: '/',
           data: {
             filter: {
               where: {
                 name: '',
               },
             },
           },
         },
         findById: {
           method: 'get',
           url: ':id',
           data: {
             id: '',
           },
         },
         create: {
           url: '/',
           data: {
             title: PropTypes.String,
             description: PropTypes.String,
             view: PropTypes.Number,
             day: PropTypes.Date,
             status: PropTypes.Boolean,
           },
         },
         update: {
           url: '/',
           data: {},
         },
         deleteById: {
           url: ':id',
           data: {
             id: '',
           },
         },
         count: {
           url: '/count',
           data: {
             filter: {
               limit: 10,
             },
           },
         },
         login: {
           url: '/login',
           data: {},
         },
       }, props.methods || {}),
    );
    model.setToken = (tokenId) => {
      config.set('access_token', tokenId);
    };
    model.props = props;
    return model;
  }
}
