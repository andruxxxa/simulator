import Model from '../Model';
const Team = new Model({
  modelName: 'Teams',
  methods: {
    addAccount: {
      url: ':teamId/accounts/rel/:userId',
      method: 'PUT', // option
      data: {
      },
    },
    getAccounts: {
      url: ':teamId/accounts',
      method: 'GET', // option
      data: {
      },
    },
  },
});
console.log(Team.addAccount, 'addAccount');
window.Team = Team;
export default Team;
