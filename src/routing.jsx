import * as React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux'
import cookies from 'browser-cookies';
import AdminLayout from './containers/AdminLayout';
import Layout from './containers/Layout2';
import Login from './containers/Login';
import Registration from './containers/Registration';
import Regions from './containers/Regions';
import Teams from './containers/Teams';
import AdminTeams from './containers/Teams/admin';
import AdminUsers from './containers/Users/admin';
import DashBoard from './containers/DashBoard';
import TeamEdit from './containers/Teams/edit';

function checkPermissions(nextState, replace, next) {
  const token = cookies.get('tokenId');
  console.log({ token })
  if (nextState.location.pathname === '/login' && token) {
    replace('/');
  } else if (nextState.location.pathname !== '/login' && !token) {
    replace('/login');
    return next();
  }
  next();
  return null;
}

const Routung = () => (
  <Router history={browserHistory}>
    <Route path="/" component={Layout} onEnter={checkPermissions}>
      <IndexRoute component={DashBoard} />
      <Route path="/teams" component={Teams} />
    </Route>
    <Route path="/admin" component={AdminLayout} onEnter={checkPermissions}>
      <IndexRoute component={DashBoard} />
      <Route path="/admin/regions" component={Regions} />
      <Route path="/admin/teams" component={AdminTeams} />
      <Route path="/admin/teams/:id" component={TeamEdit} />
      <Route path="/admin/users" component={AdminUsers} />
    </Route>
    <Route path="/login" component={Login} />
    <Route path="/registration" component={Registration} />
    <Route path="/test" component={Layout} />
  </Router>
);

export default Routung;
