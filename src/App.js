import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Routing from './routing';
import Models from './models';
import { Role, Account } from './models';
import store from './store';
import logo from './logo.svg';
import Notifications from 'react-notify-toast';
import { HashLoader } from 'react-spinners';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inited: false,
    };
    setTimeout(() => {
      return Promise.all([
        Role.init(),
        Account.init(),
      ])
      .then(() => {
        this.setState({ inited: true });
      });
    }, 1000);
  }
  render() {
    return (

      <div>
        {this.state.inited && (
        <Provider store={store}>
          <Routing />
        </Provider>
        )}
        {!this.state.inited && (
        <div className="absolute-center">
          <HashLoader
            color={'#ffffff'}
            loading={!this.state.inited}
          />
        </div>
          )}
        <Notifications />
      </div>
    );
  }
}

export default App;
