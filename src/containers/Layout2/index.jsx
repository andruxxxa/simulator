import React, { Component } from 'react';
import Sidebar from './sidebar';
import Header from './header';
import NavPanel from './NavPanel';
import Content from './content';
import jQuery from 'jquery';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
  }
  onLogout() {
    console.log('onLogout');
  }
  componentDidMount() {
    console.log(jQuery);
    jQuery(document).ready(() => {
      jQuery('.scale__real-input').on('change', function () {
        jQuery(this).parents('.scale').find('label').removeClass('scale__label_state_active');
        jQuery(this).parent().addClass('scale__label_state_active');
        jQuery(this).parents('.scale').find('.scale__block').removeClass('scale__state_option_active');
        jQuery(this).parents('.scale').find('.scale__line').removeClass('scale__state_option_active');
        jQuery(this).parents('.scale__block').prevAll().addClass('scale__state_option_active');
      });
      jQuery('.marketing-list__name').click(function () {
        jQuery(this).siblings('.marketing-list__box-model-container').slideToggle();
      });

      jQuery('.left-side-btn').click(function () {
        jQuery('.left-side').toggleClass('left-side__mobile');
        jQuery(this).find('img').toggleClass('left-side-btn__rotate');
        jQuery(this).toggleClass('left-side__opened');
      });

      if (jQuery('.form__select').length) {
        jQuery('.form__select').niceSelect();
      }

      if (jQuery('.left-side-body_type_scroll').length) {
        jQuery('.left-side-body_type_scroll').perfectScrollbar({
          suppressScrollX: true,
        });
      }

      if (jQuery('.data-table:not(.model-table)').length) {
        jQuery('.data-table:not(.model-table)').perfectScrollbar();
      }

      if (jQuery('.form__list-cnt').length) {
        jQuery('.form__list-cnt').perfectScrollbar();
      }

      if (jQuery('.list-data__inner').length) {
        jQuery('.list-data__inner').perfectScrollbar({
          suppressScrollX: true,
        });
      }
      jQuery('.widget__title').on('click', function (e) {
        e.preventDefault();

        if (e.target == this ||
         e.target == jQuery(this).find('.widget__title-arrow')[0] ||
         e.target == jQuery(this).find('.widget__title-arrow-ico')[0] ||
         e.target == jQuery(this).find('.widget__title-txt')[0]) {
          jQuery(this).parent().toggleClass('widget_close');
          jQuery(this).next().slideToggle();
          jQuery(this).parents('.widget').toggleClass('widget_height_auto');
          jQuery(this).find('.widget__title-arrow-ico').toggleClass('rotate');
        }
      });
      jQuery('.left-nav__title').click(function () {
        jQuery(this).next().slideToggle('slow');
      });
      // jQuery('.right-side__body-inner').perfectScrollbar({
      //   suppressScrollX: true,
      // });

      jQuery('.nav-panel__item_location_rightside').click(function (e) {
        e.preventDefault();
        jQuery('.nav-panel__item_location_rightside').removeClass('active-tab');
        jQuery(this).addClass('active-tab');
        jQuery('.right-side__container').removeClass('active-right-side');
        if (jQuery('#chat-switch').hasClass('active-tab')) {
          jQuery('#chat').addClass('active-right-side');
        }
        if (jQuery('#news-switch').hasClass('active-tab')) {
          jQuery('#news').addClass('active-right-side');
        }
        openRightSide();
      });


      jQuery('.nav-panel__btn').click(() => {
        const rightSide = jQuery('.right-side').width();
        jQuery('.nav-panel__icon').toggleClass('rotate-tab-icon');
        if (rightSide) {
          closeRightSide();
        } else {
          jQuery('.nav-panel__item_location_rightside').removeClass('active-tab');
          jQuery('#chat-switch').addClass('active-tab');
          jQuery('#chat').addClass('active-right-side');
          openRightSide();
        }
      });
      (function () {
        const marketingModels = jQuery('.marketing-list__box-model');
        if (marketingModels.length) {
          marketingModels.owlCarousel({
            loop: true,
            items: 1,
            nav: true,
            dots: false,
            touchDrag: false,
            mouseDrag: false,
            navText: ['<img src="img/chevron-left.png"/>', '<img src="img/chevron-right.png"/>'],
          });
        }
      }());
      leftSideSetPosition();
      jQuery(window).on('resize', () => {
        if (jQuery(window).innerWidth() > 1199) {
          jQuery('.left-side').removeClass('left-side__mobile');
          jQuery('.left-side-btn').find('img').removeClass('left-side-btn__rotate');
          jQuery('.left-side-btn').removeClass('left-side__opened');
        }
        removeRotate();
        setWidth();
        setRightSideWidth();
        leftSideSetPosition();
      });
      jQuery('.modal').on('show.bs.modal', (e) => {
        console.count();
        setRightSideWidth();
        setWidth();
      });

      jQuery('.left-side-title__block').click(function () {
        jQuery('.left-side-title__icon').toggleClass('rotate');
        jQuery(this).parent().next().slideToggle();
      });
      removeRotate();
      jQuery('.modal-help__question-title').click(function () {
        jQuery(this).next().slideToggle();
        jQuery(this).toggleClass('active-question');
      });
      setWidth();
      jQuery('.cabinet-widget__tab-btn').click(function () {
        jQuery('.cabinet-widget__tab-btn').removeClass('cabinet-widget__tab-btn_state_active');
        jQuery(this).addClass('cabinet-widget__tab-btn_state_active');

        if (jQuery(this).attr('id') == 'decision') {
          jQuery('.cabinet-widget__tab').removeClass('cabinet-widget__tab_state_active');
          jQuery('#table-content').addClass('cabinet-widget__tab_state_active');
        }
        if (jQuery(this).attr('id') == 'video-learning') {
          jQuery('.cabinet-widget__tab').removeClass('cabinet-widget__tab_state_active');
          jQuery('#video-learning-content').addClass('cabinet-widget__tab_state_active');
        }
        if (jQuery(this).attr('id') == 'video-analitic') {
          jQuery('.cabinet-widget__tab').removeClass('cabinet-widget__tab_state_active');
          jQuery('#video-analitic-content').addClass('cabinet-widget__tab_state_active');
        }
      });
    });

    function openRightSide() {
      const windowWidth = window.innerWidth;
      const leftSideWidth = jQuery('.left-side').width();
      const rightSide = jQuery('.right-side').width();
      const rightNavPanel = jQuery('.nav-panel__list_type_content').width();

      if (windowWidth > 1199) {
        jQuery('.right-side').animate({
          opacity: 1,
          width: String(rightNavPanel),
        }, 300, () => {

        });
        jQuery('.content').animate({
          width: String(jQuery('body').width() - leftSideWidth - rightNavPanel),
        }, 300);
      } else {
        jQuery('.right-side').animate({
          opacity: 1,
          width: String(rightNavPanel),
        }, 300, () => {

        });
      }
    }

    function closeRightSide() {
      jQuery('.nav-panel__item_location_rightside').removeClass('active-tab');
      jQuery(this).find('.nav-panel__icon').removeClass('rotate-tab-icon');

      const rightSide = jQuery('.right-side').width();
      const windowWidth = window.innerWidth;
      const leftSideWidth = jQuery('.left-side').width();
      if (rightSide) {
        if (windowWidth > 1199) {
          jQuery('.right-side').animate({
            width: '0',
          }, 300);
          jQuery('.content').animate({
            width: String(jQuery('body').width() - leftSideWidth),
          }, 300);
        } else {
          jQuery('.right-side').animate({
            width: '0',
          }, 300);
        }
        // jQuery('.right-side__container').removeClass('active-right-side');
      }
    }

    function setRightSideWidth() {
      const rightSide = jQuery('.right-side');
      const rightNavPanel = jQuery('.nav-panel__list_type_content').width();
      const windowWidth = window.innerWidth;
      if (rightSide.width()) {
        rightSide.width(rightNavPanel);
      }
    }

// function setHeight() {

//     var header = jQuery('.header').height();
//     var navPanel = jQuery('.nav-panel').height() ? jQuery('.nav-panel').height() : 0;
//     jQuery('.right-side__body-inner').height(jQuery(window).height() - header - navPanel - 300);

// }

    function setWidth() {
      const windowWidth = window.innerWidth;
      const leftSideWidth = jQuery('.left-side').width() ? jQuery('.left-side').width() : 0;
      const rightSide = jQuery('.right-side').width() ? jQuery('.right-side').width() : 0;
      const rightNavPanel = jQuery('.nav-panel__list_type_content').width() ? jQuery('.nav-panel__list_type_content').width() : 0;
      if (windowWidth >= 1200) {
        jQuery('.nav-panel').width(jQuery('body').width() - leftSideWidth);
        if (rightSide) {
          jQuery('.content').width(jQuery('body').width() - leftSideWidth - rightNavPanel);
        } else {
          jQuery('.content').width(jQuery('body').width() - leftSideWidth);
        }
      } else {
        jQuery('.content').width('100%');
        jQuery('.nav-panel').width('100%');
      }
    }

    function removeRotate() {
      if (jQuery(window).width() > 1199) {
        jQuery('.left-side-title__icon').removeClass('rotate');
      }
    }


    function leftSideSetPosition() {
      if (window.innerWidth < 1200) {
        const header = jQuery('.header').innerHeight();
        jQuery('.left-side').css('top', String('jQuery{header}px'));
        jQuery('.left-side-btn').css('top', String('jQuery{header}px'));
      }
    }

    function addCount(selector, maxValue, interval) {
      let count = jQuery(selector).siblings('.spinner-input__field').val();
      count = parseInt(count, 10) + interval;
      if (count <= maxValue) {
        jQuery(selector).siblings('.spinner-input__field').val(count);
      } else {
        return false;
      }
    }

    function minusCount(selector, minValue, interval) {
      let count = jQuery(selector).siblings('.spinner-input__field').val();
      count = parseInt(count, 10) - interval;
      if (count >= minValue) {
        jQuery(selector).siblings('.spinner-input__field').val(count);
      } else {
        return false;
      }
    }
  }
  render() {
    return (
      <div className="page">
        <Header />
        <Sidebar />
        <NavPanel />
        <Content />
        {/* <div id="body">*/}
        {/* <div className="container-fluid">*/}
        {/* {this.props.children}*/}
        {/* </div>*/}
        {/* </div>*/}
        {/* <Footer /> */}
      </div>);
  }
}


export default Layout;
