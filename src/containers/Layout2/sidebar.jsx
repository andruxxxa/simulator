import * as React from 'react';

import {
  Sidebar,
} from '@sketchpixy/rubix';

import { withRouter } from 'react-router';

export class SidebarContainer extends React.Component {
  render() {
    return (
      <div>
        <div className="left-side-btn"> <img className="left-side-btn__img" src="img/arrow-right.png" /></div>
        <div className="left-side left-side_page_index">
          <div className="left-side-title">
            <div className="left-side-title__block">Персонал<img className="left-side-title__icon" src="img/chevron-down.png" /></div>
          </div>
          <div className="left-side__list">
            <a className="left-side__list-link" href="">
              <div className="left-side__list-item">Продукт</div></a>
            <a className="left-side__list-link" href="marketing.html">
              <div className="left-side__list-item">Маркетинг</div>
            </a>
            <a className="left-side__list-link" href="personnel.html">
              <div className="left-side__list-item">Персонал</div>
            </a>
            <a className="left-side__list-link" href="finance.html">
              <div className="left-side__list-item">Финансы</div>
            </a>
            <a className="left-side__list-link" href="model.htm">
              <div className="left-side__list-item">Производство</div>
            </a>
            <a className="left-side__list-link" href="social.html">
              <div className="left-side__list-item">Социальная ответственность</div>
            </a>
          </div>
          <div className="left-side__body left-side-body_size_l">
            <div className="left-side__item left-side__item_type_option">
              <div className="left-side__inner left-side__inner_type_option">
                <div className="left-side__title-block left-side__title-block_size_l">
                  <div className="left-side__name">Покупка  новых линий </div>
                  <div className="left-side__description">Мин: 1 смена, <br /> Макс: 3 смены</div>
                </div>
                <div className="spinner-input spinner-input_position_middle spinner-input_size_m">
                  <div className="spinner-input__field-container spinner-input__field-container_size_l">
                    <input className="spinner-input__field" type="text" value="1" readOnly="readonly" /><img className="spinner-input__btn spinner-input__btn_arrow_up" src="img/spinner-up.png" alt="icon" /><img className="spinner-input__btn spinner-input__btn_arrow_bottom" src="img/spinner-down.png" alt="icon" />
                  </div>
                  <div className="spinner-input__currency">шт.</div>
                </div>
              </div>
            </div>
            <div className="left-side__item left-side__item_type_option">
              <div className="left-side__inner left-side__inner_type_option">
                <div className="left-side__title-block left-side__title-block_size_l">
                  <div className="left-side__name">Инвестиции производства </div>
                </div>
                <div className="spinner-input spinner-input_position_middle spinner-input_size_m">
                  <div className="spinner-input__field-container spinner-input__field-container_size_l">
                    <input className="spinner-input__field" type="text" value="3300" readOnly="readonly" /><img className="spinner-input__btn spinner-input__btn_arrow_up" src="img/spinner-up.png" alt="icon" /><img className="spinner-input__btn spinner-input__btn_arrow_bottom" src="img/spinner-down.png" alt="icon" />
                  </div>
                  <div className="spinner-input__currency">тыс. $</div>
                </div>
              </div>
            </div>
            <div className="left-side__item left-side__item_type_option">
              <div className="left-side__inner left-side__inner_type_option">
                <div className="left-side__title-block left-side__title-block_size_l">
                  <div className="left-side__name">Загрузка оборудования</div>
                </div>
                <div className="scale scale_size_m">
                  <div className="scale__inner">
                    <div className="scale__block scale__block_option_main">
                      <label className="scale__label_option_main">
                        <input className="scale__real-input" type="radio" name="personal" value="0" hidden="hidden" /><span className="scale__radio-input scale__radio-input_option_main" />
                      </label>
                      <div className="scale__tooltip">0%</div>
                    </div>
                    <div className="scale__line scale__line_option_main" />
                    <div className="scale__block scale__block_option_main">
                      <label className="scale__label_option_main">
                        <input className="scale__real-input" type="radio" name="personal" value="0" hidden="hidden" /><span className="scale__radio-input scale__radio-input_option_main" />
                      </label>
                      <div className="scale__tooltip">25%</div>
                    </div>
                    <div className="scale__line scale__line_option_main" />
                    <div className="scale__block scale__block_option_main">
                      <label className="scale__label_option_main">
                        <input className="scale__real-input" type="radio" name="personal" value="0" hidden="hidden" /><span className="scale__radio-input scale__radio-input_option_main" />
                      </label>
                      <div className="scale__tooltip">50%</div>
                    </div>
                    <div className="scale__line scale__line_option_main" />
                    <div className="scale__block scale__block_option_main">
                      <label className="scale__label_option_main">
                        <input className="scale__real-input" type="radio" name="personal" value="0" hidden="hidden" /><span className="scale__radio-input scale__radio-input_option_main" />
                      </label>
                      <div className="scale__tooltip">75%</div>
                    </div>
                    <div className="scale__line scale__line_option_main" />
                    <div className="scale__block scale__block_option_main">
                      <label className="scale__label_option_main">
                        <input className="scale__real-input" type="radio" name="personal" value="0" hidden="hidden" /><span className="scale__radio-input scale__radio-input_option_main" />
                      </label>
                      <div className="scale__tooltip">100%</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(SidebarContainer);
