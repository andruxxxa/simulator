import * as React from 'react';

import {
  Sidebar,
} from '@sketchpixy/rubix';

import { withRouter } from 'react-router';

export class NavPanel extends React.Component {
  render() {
    return (
      <div className="nav-panel">
        <div className="nav-panel__list nav-panel__list_type_options"><a className="nav-panel__item nav-panel__item_location_leftside" href="#">Показатели</a><a className="nav-panel__item nav-panel__item_location_leftside" href="#">Ключевые показатели</a><a className="nav-panel__item nav-panel__item_location_leftside" href="#">Отчетность</a>
        </div>
        <div className="nav-panel__list nav-panel__list_type_content"><a className="nav-panel__item nav-panel__item_location_rightside" href="#" id="chat-switch">Чат<span className="nav-panel__count">3</span></a><a className="nav-panel__item nav-panel__item_location_rightside" href="#" id="news-switch">Новости<span className="nav-panel__count">18</span></a></div>
        <button className="nav-panel__btn" type="button"><img className="nav-panel__icon" src="img/arrow-left.png" alt="icon" /></button>
      </div>
    );
  }
}

export default withRouter(NavPanel);
