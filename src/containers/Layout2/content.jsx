import React, { Component } from 'react';
import _ from 'lodash';
import Widget from '../../components/widget';
import Table from '../../components/table';
import SmartTable from '../../components/SmartTable';
import Region from '../../models/Regions';
class Content extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="all-personnel content">
        <div className="content__cols">
          <div className="content__col">
            <Widget title="Тестовая таблица" description="Пояснение">
              <Table
                data={_.range(10).map((i) => {
                  return {
                    id: i + 1,
                    name: `Тест ${i + 1}`,
                  };
                })}
              />
            </Widget>
          </div>
          <div className="content__col">
            <Widget>
              <SmartTable
                model={Region}
                columns={[
                  {
                    title: 'ID',
                    options: {
                      isKey: true,
                      dataField: 'id',
                      filter: {
                        type: 'TextFilter',
                      },
                      dataSort: true,
                    },
                  },
                  {
                    title: 'title',
                    options: {
                      dataField: 'title',
                      filter: {
                        type: 'TextFilter',
                      },
                    },
                  },
                ]}
              />
            </Widget>
          </div>
        </div>
      </div>
    );
  }
}


export default Content;
