import React, { Component } from 'react';
import {
  Modal,
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import Region from '../../models/Regions';
import Account from '../../models/Account';
export default class AddTeamModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.getAccounts = this.getAccounts.bind(this);
    this.getAccounts();
  }
  handleChange(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }
  getAccounts() {
    return Account
    .find()
    .then((accounts) => {
      if (!Array.isArray(accounts)) return;
      this.setState({ accounts });
    });
  }
  onClose() {
    this.setState({ userId: null });
    this.props.onClose();
  }
  onSubmit() {
    this.props.onSubmit(this.state);
    this.setState({ userId: null });
  }
  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Добавление команды</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup bsSize="large">
              <FormControl componentClass="select" name="userId" onChange={this.handleChange}>
                {this.state.accounts.map((account) => {
                  return (<option value={account.id}>{Account.getName(account)}</option>);
                })}
              </FormControl>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.onClose}>Закрыть</Button>
          <Button onClick={this.onSubmit} bsStyle="primary">Добавить</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
