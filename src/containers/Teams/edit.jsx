import React, { Component } from 'react';
import moment from 'moment/min/moment-with-locales.min';
import {
  Table,
  FormGroup,
  Button,
  Col,
  PanelContainer,
  PanelBody,
  Grid,
  Row,
  Panel,
  PanelFooter,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import Team from '../../models/Teams';
import Region from '../../models/Regions';
import Account from '../../models/Account';
import AddTeamModal from './addModal';
import AddAccount from './addAccount';
export default class TeamsEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      team: false,
      teamId: this.props.routeParams.id,
      accounts: [],
    };
    this.getTeam = this.getTeam.bind(this);
    this.getAccounts = this.getAccounts.bind(this);
    this.getTeam();
    this.getAccounts();
  }
  getTeam() {
    return Team.findById(this.state.teamId)
    .then((team) => {
      console.log('getTeam', team);
      this.setState({ team });
    });
  }
  getAccounts() {
    return Team.getAccounts(this.state.teamId)
    .then((accounts) => {
      if (!Array.isArray(accounts)) return;
      this.setState({ accounts });
    });
  }
  render() {
    const { team } = this.state;
    return (<div>
      <PanelContainer>
        <Panel>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12}>
                  <h3>Команда "{team.name}"</h3>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
          <PanelFooter className="bg-white">
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <p>
                    <span>
                      Создана: {moment(team.created).locale('ru').format('LLL')}
                    </span>
                  </p>
                </Col>
              </Row>
            </Grid>
          </PanelFooter>
        </Panel>
      </PanelContainer>
      <PanelContainer>
        <Panel>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12}>
                  <h3>Участники</h3>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
          <PanelFooter className="bg-white">
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <Table striped bordered condensed hover style={{ backgroundColor: 'white' }}>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>ФИО</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.accounts.map((account) => {
                        return (
                          <tr>
                            <td>{account.id}</td>
                            <td>{account.email}</td>
                            <td>{Account.getName(account)}</td>
                          </tr>);
                      })}
                    </tbody>
                  </Table>
                  <FormGroup bsSize="large" className="text-right">
                    <Button onClick={this.show} lg bsStyle="primary">Добавить</Button>
                  </FormGroup>
                  <AddAccount showModal={this.state.showModal} onClose={this.close} onSubmit={this.add} />
                </Col>
              </Row>
            </Grid>
          </PanelFooter>
        </Panel>
      </PanelContainer>
      {/* <FormGroup bsSize="large" className="text-right">*/}
      {/* <Button onClick={this.show} lg bsStyle="primary">Добавить</Button>*/}
      {/* </FormGroup>*/}
      {/* <AddTeamModal showModal={this.state.showModal} onClose={this.close} onSubmit={this.add} />*/}
    </div>);
  }
}
