import React, { Component } from 'react';
import cookies from 'browser-cookies';
import Notify from '../../components/notify';
import validator from 'validator';
import LoginForm from './LoginForm';
import './style.css';
import Account from '../../models/Account';
import Role from '../../models/Role';
import store from '../../store';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit({ email, password }) {
    if (!validator.isEmail(email)) {
      return Notify('Email введен некорректно');
    }
    if (!password) {
      return Notify('Введите пароль');
    }
    return Account.login({ email, password })
    .then((res) => {
      if (!res.success) {
        return Notify('Не удалось авторизоваться');
      }
      Account.setToken(res.id);
      return Account
      .findById(res.userId)
      .then((user) => {
        store.dispatch({ type: 'ACCOUNTS_LOGIN', data: { user, tokenId: res.id } });
        const { router } = this.props;

        const role = Role.getById(user.roleId);
        console.log(role);
        if (role && role.name === 'moderator') {
          return router.push('/admin');
        }
        return router.push('/');
      });
    });
  }

  render() {
    return (
      <div className="auth-container">
        <LoginForm onSubmit={this.onSubmit} />
      </div>
    );
  }
}
export default Login;
