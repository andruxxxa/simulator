import React, { Component } from 'react';
import { PanelContainer,
  Panel,
  PanelHeader,
  PanelBody,
  Grid,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroup,
  FormControl,
  Icon,
  Button,
} from '@sketchpixy/rubix';
import { Link } from 'react-router';

export class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }
  handleChange(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className="bg-blue">
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <h3 className="text-center" >Авторизация</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <Form onSubmit={this.onSubmit}>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="mail" />
                        </InputGroup.Addon>
                        <FormControl
                          name="email"
                          placeholder="Логин"
                          type="mail"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="key" />
                        </InputGroup.Addon>
                        <FormControl
                          name="password"
                          type="password"
                          placeholder="Пароль"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large" className="text-center">
                      <Link to="/registration"> У меня еще нет аккаунта</Link>
                    </FormGroup>
                    <FormGroup bsSize="large" className="text-right">
                      <Button type="submit" lg bsStyle="primary">Войти</Button>
                    </FormGroup>
                  </Form>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    );
  }
}
export default LoginForm;
