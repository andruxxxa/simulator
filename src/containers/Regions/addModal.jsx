import React, { Component } from 'react';
import {
  Modal,
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
} from '@sketchpixy/rubix';
export default class AddRegionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onClose = this.onClose.bind(this);
  }
  onClose() {
    this.setState({ title: '' });
    this.props.onClose();
  }
  onSubmit() {
    const state = Object.assign({}, this.state);
    this.props.onSubmit(state);
    this.setState({ title: '' });
  }
  handleChange(e) {
    this.setState({ title: e.target.value });
  }
  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Добавление региона</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup>
              <ControlLabel>Название</ControlLabel>
              <FormControl
                type="text"
                value={this.state.title}
                placeholder="Название региона"
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.onClose}>Закрыть</Button>
          <Button onClick={this.onSubmit} bsStyle="primary">Добавить</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
