import React, { Component } from 'react';
import {
  Table,
  FormGroup,
  Button,
} from '@sketchpixy/rubix';
import Region from '../../models/Regions';
import AddRegionModal from './addModal';
import store from '../../store';
export default class Regions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      regions: [],
    };
    this.show = this.show.bind(this);
    this.close = this.close.bind(this);
    this.add = this.add.bind(this);
    this.update = this.update.bind(this);
    this.update();
  }
  update() {
    return Region
    .find()
    .then((regions) => {
      this.setState({ regions });
    });
  }
  add(data) {
    this.close();
    return Region
    .create(data)
    .then(this.update);
  }
  show() {
    this.setState({ showModal: true });
  }
  close() {
    this.setState({ showModal: false });
  }
  render() {
    return (<div>
      <Table striped bordered condensed hover style={{ backgroundColor: 'white' }}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Название</th>
          </tr>
        </thead>
        <tbody>
          {this.state.regions.map((region) => {
            return (
              <tr>
                <td>{region.id}</td>
                <td>{region.title}</td>
              </tr>);
          })}
        </tbody>
      </Table>
      <FormGroup bsSize="large" className="text-right">
        <Button onClick={this.show} lg bsStyle="primary">Добавить</Button>
      </FormGroup>
      <AddRegionModal showModal={this.state.showModal} onClose={this.close} onSubmit={this.add} />
    </div>);
  }
}
