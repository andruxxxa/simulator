import React, { Component } from 'react';
import { notify } from 'react-notify-toast';
import Account from '../../models/Account';
import Role from '../../models/Role';
import RegistrationForm from './RegistrationForm';
import store from '../../store';
import './style.css';
export default class Registration extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(values) {
    // console.log(values);
    const accountData = Object.assign({}, values);
    const role = Role.getByName(values.roleName);
    if (role) {
      accountData.roleId = role.id;
    }
    let username = '';
    if (accountData.lastName) {
      username += accountData.lastName;
    }
    if (accountData.firstName) {
      if (username) username += ' ';
      username += accountData.firstName;
    }
    if (accountData.middleName) {
      if (username) username += ' ';
      username += accountData.middleName;
    }
    delete accountData.roleName;
    let myColor = { background: '#0E1717', text: "#FFFFFF" };
    return Account.create(accountData)
    .then((res) => {
      console.log(res);
      notify.show('Регистрация прошло успешно!', 'custom', 2000, myColor);
      setTimeout(() => {
        notify.show('Теперь вы можете войти под своим аккаунтом', 'custom', 4000, myColor);
      }, 2500);
      const { router } = this.props;
      return router.push('/login');
    });
  }

  render() {
    return (
      <div className="auth-container bg-papaya-dark">
        <RegistrationForm onSubmit={this.onSubmit} />
      </div>
    );
  }
}
