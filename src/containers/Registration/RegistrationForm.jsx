import React, { Component } from 'react';
import { Link } from 'react-router';
import {
  PanelContainer,
  Panel,
  PanelHeader,
  PanelBody,
  Grid,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroup,
  FormControl,
  Icon,
  Button,
} from '@sketchpixy/rubix';

export default class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      roleName: 'player',
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }

  handleChange(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }

  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className="bg-blue">
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <h3 className="text-center">Регистрация</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12} className="fg-white">
                  <Form onSubmit={this.onSubmit}>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="mail" />
                        </InputGroup.Addon>
                        <FormControl
                          onChange={this.handleChange}
                          name="email"
                          placeholder="e-mail"
                          type="mail"
                        />
                      </InputGroup>
                    </FormGroup>

                    {/* <FormGroup bsSize="large">*/}
                    {/* <InputGroup>*/}
                    {/* <InputGroup.Addon>*/}
                    {/* <Icon bundle="fontello" glyph="mail" />*/}
                    {/* </InputGroup.Addon>*/}
                    {/* <FormControl*/}
                    {/* onChange={this.handleChange}*/}
                    {/* name="lastName"*/}
                    {/* placeholder="Фамилия"*/}
                    {/* type="text"*/}
                    {/* />*/}
                    {/* </InputGroup>*/}
                    {/* </FormGroup>*/}
                    {/* <FormGroup bsSize="large">*/}
                    {/* <InputGroup>*/}
                    {/* <InputGroup.Addon>*/}
                    {/* <Icon bundle="fontello" glyph="mail" />*/}
                    {/* </InputGroup.Addon>*/}
                    {/* <FormControl*/}
                    {/* onChange={this.handleChange}*/}
                    {/* name="firstName"*/}
                    {/* placeholder="Имя"*/}
                    {/* type="text"*/}
                    {/* />*/}
                    {/* </InputGroup>*/}
                    {/* </FormGroup>*/}
                    {/* <FormGroup bsSize="large">*/}
                    {/* <InputGroup>*/}
                    {/* <InputGroup.Addon>*/}
                    {/* <Icon bundle="fontello" glyph="mail" />*/}
                    {/* </InputGroup.Addon>*/}
                    {/* <FormControl*/}
                    {/* onChange={this.handleChange}*/}
                    {/* name="fatherName"*/}
                    {/* placeholder="Отчество"*/}
                    {/* type="text"*/}
                    {/* />*/}
                    {/* </InputGroup>*/}
                    {/* </FormGroup>*/}

                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="key" />
                        </InputGroup.Addon>
                        <FormControl
                          onChange={this.handleChange}
                          name="password"
                          type="password"
                          placeholder="Пароль"
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="pen" />
                        </InputGroup.Addon>
                        <FormControl
                         name="lastName"
                         type="text"
                         placeholder="Фамилия"
                         onChange={this.handleChange}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="pen" />
                        </InputGroup.Addon>
                        <FormControl
                         name="firstName"
                         type="text"
                         placeholder="Имя"
                         onChange={this.handleChange}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large">
                      <InputGroup>
                        <InputGroup.Addon>
                          <Icon bundle="fontello" glyph="pen" />
                        </InputGroup.Addon>
                        <FormControl
                         name="middleName"
                         type="text"
                         placeholder="Отчество"
                         onChange={this.handleChange}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup bsSize="large">
                      <FormControl componentClass="select" name="roleName" onChange={this.handleChange}>
                        <option value="player">Игрок</option>
                        <option value="moderator">Модератор</option>
                      </FormControl>
                    </FormGroup>
                    <FormGroup bsSize="large" className="text-center">
                      <Link to="/login">У меня уже есть аккаунт</Link>
                    </FormGroup>
                    <FormGroup bsSize="large" className="text-right">
                      <Button type="submit" lg bsStyle="primary">Зарегистрироваться</Button>
                    </FormGroup>
                  </Form>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>;
     </PanelContainer>
    );
  }
}
