import React, { Component } from 'react';
import {
  Modal,
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import Region from '../../models/Regions';
export default class AddTeamModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      regions: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.updateRegions();
  }
  handleChange(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }
  updateRegions() {
    return Region
    .find()
    .then((regions) => {
      console.log({ regions });
      regions = regions.filter(region => !!region.title);
      const regionId = _.get(regions, '[0].id');
      this.setState({ regions });
      if (regionId) {
        this.setState({ regionId });
      }
    });
  }
  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Добавление команды</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup>
              <ControlLabel>Название</ControlLabel>
              <FormControl
                type="text"
                name="name"
                placeholder="Название команды"
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup bsSize="large">
              <FormControl componentClass="select" name="regionId" onChange={this.handleChange}>
                {this.state.regions.map((region) => {
                  return (<option value={region.id}>{region.title}</option>);
                })}
              </FormControl>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onClose}>Закрыть</Button>
          <Button onClick={() => this.props.onSubmit({ name: this.state.name, regionId: this.state.regionId })} bsStyle="primary">Добавить</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
