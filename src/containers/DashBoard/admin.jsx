import React, { Component } from 'react';
import {
  Table,
  FormGroup,
  Button,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import Team from '../../models/Teams';
import Region from '../../models/Regions';
import AddTeamModal from './addModal';
export default class Teams extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teams: [],
    };
    this.show = this.show.bind(this);
    this.close = this.close.bind(this);
    this.add = this.add.bind(this);
    this.getRegionTitleById = this.getRegionTitleById.bind(this);
    this.updateTeams = this.updateTeams.bind(this);
    this.updateRegions = this.updateRegions.bind(this);
    this.getRegionTitleById = this.getRegionTitleById.bind(this);
    this.updateTeams();
    this.updateRegions();
  }
  getRegionTitleById(regionId) {
    if (!regionId) return '';
    const region = _.find(this.state.regions, { id: regionId });
    if (!region) return '';
    return region.title;
  }
  updateTeams() {
    return Team
    .find()
    .then((teams) => {
      this.setState({ teams });
    });
  }
  updateRegions() {
    return Region
    .find()
    .then((regions) => {
      this.setState({ regions });
    });
  }
  add(data) {
    this.close();
    return Team
    .create(data)
    .then(this.updateTeams);
  }
  show() {
    this.setState({ showModal: true });
  }
  close() {
    this.setState({ showModal: false });
  }
  render() {
    return (<div>
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Регион</th>
          </tr>
        </thead>
        <tbody>
          {this.state.teams.map((team) => {
            return (
              <tr>
                <td>{team.id}</td>
                <td>{team.name}</td>
                <td>{this.getRegionTitleById(team.regionId)}</td>
              </tr>);
          })}
        </tbody>
      </Table>
      <FormGroup bsSize="large" className="text-right">
        <Button onClick={this.show} lg bsStyle="primary">Добавить</Button>
      </FormGroup>
      <AddTeamModal showModal={this.state.showModal} onClose={this.close} onSubmit={this.add} />
    </div>);
  }
}
