import React, { Component } from 'react';
import {
  Table,
  PanelContainer,
  Panel,
  PanelBody,
  Grid,
  Row,
  Col,
  LoremIpsum,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import moment from 'moment/min/moment-with-locales.min';
import store from '../../store';
import Role from '../../models/Role';
import Account from '../../models/Account';
export default class Teams extends Component {
  constructor(props) {
    super(props);
    const { accounts } = store.getState();
    console.log(accounts, 111);
    this.state = {
      user: accounts.user,
    };
    this.getRoleName = this.getRoleName.bind(this);
  }
  getRoleName() {
    const role = Role.getById(this.state.user.roleId);
    let roleName = _.get(role, 'name', '');
    // const roleName = Role.getById(this.state.user.roleId).name;
    // console.log({ roleName });
    if (roleName === 'moderator') {
      return 'Модератор';
    } else if (roleName === 'player') {
      return 'Игрок';
    }
    return roleName;
  }
  render() {
    const { user } = this.state;
    return (<div>
      <PanelContainer>
        <Panel>
          <PanelBody style={{ padding: 0 }}>
            <Grid>
              <Row>
                {user && (
                <Col xs={12}>
                  <h3>Добро пожаловать, {Account.getName(user)}!</h3>
                  <p>
                    <span>
                       Ваш email: {user.email}
                    </span>
                  </p>
                  <p>
                    <span>
                       Дата регистрации: {moment(user.created).locale('ru').format('LLL')}
                    </span>
                  </p>
                  <p>
                    <span>
                      Роль в системе: {this.getRoleName()}
                    </span>
                  </p>
                </Col>
                )}
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    </div>);
  }
}
