import React, { Component } from 'react';
import {
  Table,
} from '@sketchpixy/rubix';
import _ from 'lodash';
import moment from 'moment';
import Account from '../../models/Account';
export default class Teams extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
    this.updateUsers = this.updateUsers.bind(this);
    this.updateUsers();
  }
  updateUsers() {
    return Account
    .find()
    .then((users) => {
      console.log({ users })
      if (!Array.isArray(users)) return;
      this.setState({ users });
    });
  }
  render() {
    return (<div>
      <Table striped bordered condensed hover style={{ backgroundColor: 'white' }}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Почта</th>
            <th>Дата регистрации</th>
          </tr>
        </thead>
        <tbody>
          {this.state.users.map((user) => {
            return (
              <tr>
                <td>{user.id}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>{moment(user.created).format('LLL')}</td>
              </tr>);
          })}
        </tbody>
      </Table>
    </div>);
  }
}
