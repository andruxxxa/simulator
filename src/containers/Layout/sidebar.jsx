import * as React from 'react';

import {
  Sidebar,
} from '@sketchpixy/rubix';

import { withRouter } from 'react-router';

// import ClientsSidebar from '../../components/ClientsSidebar';
// import WorkersSidebar from '../../components/WorkersSidebar';
// import MapSidebar from '../../components/MapSidebar';
// import OrdersSidebar from '../../components/OrdersSidebar';

const Logo = require('./logo_short.png');

export class SidebarContainer extends React.Component<any, {}> {
  render() {
    return (
      <div id="sidebar">
        <div id="avatar">
          <img src={Logo} alt="logo" style={{ width: '70%', marginLeft: 25 }} />
        </div>
        <div id="sidebar-container">
          <Sidebar sidebar={0}>
            {/*SIDEBAR*/}
            {/*/!* <ApplicationSidebar {...this.props} />*!/*/}
          </Sidebar>
        </div>
      </div>
    );
  }
}

export default withRouter(SidebarContainer);
