import * as React from 'react';
import { shallow } from 'enzyme';
import Sidebar, { SidebarContainer } from './sidebar';
import Header from './header';
import Layout from './index';
import {
  Grid
} from '@sketchpixy/rubix';

describe('<Layout />', () => {
  it('should be rendered', () => {
    const layout: any = shallow(<Layout>test</Layout>);
    expect(layout.find(Header).length).toEqual(1);
    expect(layout.find(Sidebar).length).toEqual(1);
    expect(layout.find('.container-fluid').text()).toEqual('test');
  });
  describe('<Header />', () => {
    it('should be rendered', () => {
      const header: any = shallow(<Header />);
      expect(header.is(Grid)).toEqual(true);
    });
  });
  describe('<Sidebar />', () => {
    it('should be rendered', () => {
      const sidebar: any = shallow(<SidebarContainer />);
      expect(sidebar.is('#sidebar')).toEqual(true);
    });
  });
});