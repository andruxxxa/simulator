import React, { Component } from 'react';
import Sidebar from './sidebar';
import Header from './header';
import './style.css';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
  }
  onLogout() {
    console.log('onLogout');
  }
  render() {
    return (<div className="app-container">
      <div id="container">
        {/* <Sidebar />*/}
        <Header onLogout={this.onLogout} />
        <div id="body">
          <div className="container-fluid">
            {this.props.children}
          </div>
        </div>
        {/* <Footer /> */}
      </div>
    </div>);
  }
}


export default Layout;
