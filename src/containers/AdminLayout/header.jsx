import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import {
  SidebarBtn,
  Navbar,
  Nav,
  NavItem,
  Icon,
  Grid,
  Row,
  Col,
} from '@sketchpixy/rubix';

// import profileActions from '../../actions/profile';

const Logo = require('./logo_short.png');

const Brand = () => (
 <Navbar.Header>
   <Navbar.Brand tabIndex="-1">
     <a href="/">
       RegionSim
     </a>
   </Navbar.Brand>
 </Navbar.Header>
);

const LeftNavigation = () => (
  <Nav className="hidden-xs">
    <NavItem componentClass={Link} href="/admin/regions" to="/admin/regions" onlyActiveOnIndex activeClassName="header__nav-active-link">
      <span title="Карта" className="header-nav-item">
        <span className="icon-papaya_map" />
        <span className="item-title">Регионы</span>
      </span>
    </NavItem>
    <NavItem divider />
    <NavItem componentClass={Link} href="/admin/teams" to="/admin/teams" activeClassName="header__nav-active-link">
      <span title="Команды" className="header-nav-item">
        <span className="icon-papaya_work3" />
        <span className="item-title">Команды</span>
      </span>
    </NavItem>
    <NavItem divider />
    <NavItem componentClass={Link} href="/admin/users" to="/admin/users" activeClassName="header__nav-active-link">
      <span title="Пользователи" className="header-nav-item">
        <span className="icon-papaya_orders" />
        <span className="item-title">Пользователи</span>
      </span>
    </NavItem>
    {/*<NavItem divider />*/}
    {/*<NavItem componentClass={Link} href="#" activeClassName="header__nav-active-link">*/}
      {/*<span title="Команды" className="header-nav-item">*/}
        {/*<span className="icon-papaya_work3" />*/}
        {/*<span className="item-title">Команды</span>*/}
      {/*</span>*/}
    {/*</NavItem>*/}
    {/*<NavItem divider />*/}
    {/*<NavItem componentClass={Link} href="/workers" to="/workers" activeClassName="header__nav-active-link">*/}
      {/*<span title="Работники" className="header-nav-item">*/}
        {/*<span className="icon-papaya_work1" />*/}
        {/*<span className="item-title">Работники</span>*/}
      {/*</span>*/}
    {/*</NavItem>*/}
    {/*<NavItem divider />*/}
    {/*<NavItem componentClass={Link} href="#" activeClassName="header__nav-active-link">*/}
      {/*<span title="Отчеты" className="header-nav-item">*/}
        {/*<span className="icon-papaya_reviews" />*/}
        {/*<span className="item-title">Отчеты</span>*/}
      {/*</span>*/}
    {/*</NavItem>*/}
  </Nav>
);

const RightNavigation = () => (
  <Nav pullRight className="hidden-xs">
    <NavItem componentClass={Link} href="#" activeClassName="header__nav-active-link">
      <span title="Инструкция" className="header-nav-item">
        <span className="icon-papaya_instruction" />
        <span className="item-title">Инструкция</span>
      </span>
    </NavItem>

    <NavItem divider />
    <NavItem componentClass={Link} href="#" className="header-menu menu-default">
      <span title="Карта" className="header-nav-item">
        <Icon bundle="fontello" glyph="cog-7" />
      </span>
    </NavItem>
  </Nav>
);

const LogoutNavigation = connect()(props => (
  <Nav className="pull-right">
    <NavItem
      className="logout" href="#" onClick={() => {
        props.dispatch({ type: 'ACCOUNTS_LOGOUT' });
        browserHistory.push('/login');
      }}
    >
      <Icon bundle="fontello" glyph="off-1" />
    </NavItem>
  </Nav>
));

class Header extends Component {
  render() {
    return (
      <Grid className="navbar">
        <Navbar fixedTop fluid id="rubix-nav-header">
          <Row>
            <Col xs={2} visible="xs">
              <SidebarBtn />
            </Col>
            <Col xs={8} sm={11} collapseLeft collapseRight>
              <Brand />
              <LeftNavigation />
              {/*<RightNavigation />*/}
            </Col>
            <Col xs={2} sm={1} collapseLeft collapseRight>
              <LogoutNavigation />
            </Col>
          </Row>
        </Navbar>
      </Grid>
    );
  }
}

export default Header;
