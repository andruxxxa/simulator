import React, { Component } from 'react';
import logo from '../../logo.svg';
export default class Layout extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    return (<div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
      <p className="App-intro">
        {this.props.children}
      </p>
    </div>);
  }
}
